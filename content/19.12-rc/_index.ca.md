---
title: 19.12 RC Releases
publishDate: 2019-19-28 00:01:00
layout: page # don't translate
summary: "Uns 120 programes individuals i dotzenes de biblioteques de programador i connectors de funcionalitats es publiquen simultàniament com a part del servei de llançaments del KDE."
type: announcement # don't translate
---

29 de novembre de 2019. Uns 120 programes individuals i dotzenes de
biblioteques de programador i connectors de funcionalitats es publiquen
simultàniament com a part del servei de llançaments del KDE.

Avui tots aconseguiran els codis fonts de la versió candidata del
llançament, indicant que han completat les funcionalitats però necessiten
proves per cercar errors.

Cal que els empaquetadors de les distribucions i de les botigues
d'aplicacions actualitzin els seus canals de prellançament per comprovar si
hi ha problemes.

+ [notes de llançament de
  19.12](https://community.kde.org/Releases/19.12_Release_Notes) per a
  informació quant als arxius tar i problemes coneguts.
+ [Pàgina del wiki per a la baixada de
  paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [Pàgina d'informació del codi font de la 19.12
  RC](https://kde.org/info/applications-19.11.90)

## Contactes de premsa

Per a més informació, envieu-nos un correu:
[press@kde.org](mailto:press@kde.org).
