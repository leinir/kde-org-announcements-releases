---
title: Выпуски 19.12 RC
publishDate: 2019-11-29 00:01:00
layout: page # don't translate
summary: "Более 120 отдельных программ и десятки программных библиотек и модулей одновременно выпущены силами Службы выпусков KDE."
type: announcement # don't translate
---

29 ноября 2019 года. Более 120 отдельных программ и десятки программных
библиотек и модулей одновременно выпущены силами Службы выпусков KDE.

Today they all get release candidate sources meaning they are feature
complete but need testing for final bugfixes.

Distro and app store packagers should update their pre-release channels to
check for issues.

+ [Примечания к выпуску
  19.12](https://community.kde.org/Releases/19.12_Release_Notes) содержат
  информацию о пакетах и об известных проблемах.
+ [Вики-страница об установке
  пакетов](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [Архивы с исходным кодом 19.12
  RC](https://kde.org/info/applications-19.11.90)

## Контакты для прессы

Для получения дополнительной информации свяжитесь с нами по электронной
почте: [press@kde.org](mailto:press@kde.org).
