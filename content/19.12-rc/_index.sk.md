---
title: 19.12 RC Releases
publishDate: 2019-11-29 00:01:00
layout: page # don't translate
summary: "Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service."
type: announcement # don't translate
---

29. novembra 2019. Viac ako 120 individuálnych programov plus desiatky
knižníc a doplnkov sú vydávané súčasne ako súčasť služby vydania KDE.

Dnes všetci získajú kandidátske zdroje vydania, čo znamená, že sú kompletné,
ale je potrebné otestovať ich konečné opravy.

Správcovia balíkov z Distro a App Store by mali aktualizovať svoje kanály
pred vydaním, pre kontrolu problémov.

+ [19.12 poznámky k vydaniu]
  (https://community.kde.org/Releases/19.12_Release_Notes), kde nájdete
  informácie o tarball-och a známych problémoch.
+ [Wiki stránka na stiahnutie balíkov]
  (https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [19.12 RC zdrojová informačná
  stránka](https://kde.org/info/applications-19.11.90)

## Tlačové kontakty

Pre viac informácií nám pošlite e-mail:
[press@kde.org](mailto:press@kde.org).
