---
titel: 19.12 RC Releases
publicatiedatum: 2019-11-29 00:01:00
layout: page # don't translate
summary: "Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service."
type: announcement # don't translate
---

29 november 2019. Meer dan 120 individuele programma's plus tientallen
programmeerbibliotheken en functieplug-ins zijn tegelijk vrijgegeven als
onderdeel van de vrijgaveservice van KDE.

Vandaag krijgen ze allemaal uitgavekandidaten (release candidate) van
broncode wat betekent dat ze qua functie compleet zijn maar testen nodig
hebben voor de laatste reparaties van bugs.

Distributies en pakketmakers voor applicatiewinkels zouden hun kanalen voor
vooruitgaven moeten bijwerken voor controle op problemen.

+ [19.12
  uitgavenotities](https://community.kde.org/Releases/19.12_Release_Notes)
  voor informatie over tarballs en bekende problemen.
+ [Wiki-pagina voor
  downloaden](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [19.12 RC informatiepagina
  broncode](https://kde.org/info/applications-19.11.90)

## Perscontacten

Voor meer informatie stuur ons een e-mail:
[press@kde.org](mailto:press@kde.org).
