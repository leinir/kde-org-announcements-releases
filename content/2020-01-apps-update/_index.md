---
title: KDE's January 2020 Apps Update
publishDate: 2020-12-12 13:01:00
layout: page # don't translate
summary: "What Happened in KDE's Applications This Month"
type: announcement # don't translate
---

<!--
 git diff 'HEAD@{1 month ago}' HEAD

 ssh ftpadmin@master.kde.org 'find stable/ -mtime 31'
-->

# KDE Apps Update for January

## KNewStuff

big changes in this library, see dialog in global themes

## Store Updates

## Upcoming Projects

## Releases 19.12.1

Some of our projects release on their own timescale and some get released en-masse. The 19.12.1 bundle of projects was released today and should be available through app stores and distros soon.  See the [19.12.1 releases page](https://www.kde.org/info/releases-19.12.1.php).  This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because it is dozens of different products rather than a single whole.
