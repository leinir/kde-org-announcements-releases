---
title: Actualização das Aplicações de Dezembro de 2019 do KDE
publishDate: 2019-12-12 13:01:00
layout: page # don't translate
summary: "O Que se Passou nas Aplicações do KDE Neste Mês"
type: announcement # don't translate
---

# Novas versões do KDE a chegar em Dezembro

O lançamento de novas versões das aplicações do KDE faz parte do esforço
contínuo do KDE para lhe trazer um catálogo completo e actualizado dos
diversos programas completos, bonitos e úteis para o seu sistema.

Estão agora disponíveis as novas versões do gestor de ficheiros do KDE
Dolphin; o Kdenlive, um dos editores de vídeo mais completos em código
aberto; o visualizador de documentos Okular; o visualizador de imagens do
KDE Gwenview e todas as suas aplicações e utilitários favoritos do
KDE. Todas estas aplicações têm sido melhoradas, tornando-se mais rápidas e
mais estáveis, assim como oferecem novas funcionalidades espectaculares. As
novas versões das aplicações do KDE permitem-lhe ser produtivo e criativo,
enquanto ao mesmo tempo tiram partido das aplicações do KDE de forma simples
e divertida.

Esperamos que goste de todas as novidades e melhorias aplicadas em todas as
aplicações do KDE!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan)
Recarregado

O [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan)
vai receber uma grande versão nova em Dezembro. A ferramenta de planeamento
e gestão de projectos do KDE atingiu um grande marco desde a versão
anterior, que foi lançada há quase dois anos.

{{< img class="text-center" src="kplan.png"
link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png"
caption="Calligra Plan" >}}

O Plan ajuda-o a gerir projectos pequenos e grandes com vários recursos.
Para poder modelar o seu projecto, o  Plan oferece-lhe diferentes tipos de
dependências de tarefas e restrições temporais. Poderá definir as suas
tarefas, estimar o esforço necessário para executar cada uma delas, alocar
recursos e depois agendar o projecto de acordo com o que precisa e com os
recursos disponíveis.

Uma das potencialidades do Plan é o seu excelente suporte para os diagramas
de Gantt. Os diagramas de Gantt são gráficos compostos por barras
horizontais que oferecem uma ilustração gráfica de um plano de ajuda e
calendarização, coordenar e acompanhar tarefas específicas num projecto. O
uso de diagramas de Gantt no Plan ajudá-lo-á a monitorizar melhor o
funcionamento do seu projecto.

## Aumente o Volume no [Kdenlive](https://kdenlive.org)

Os programadores do [Kdenlive](https://kdenlive.org) têm estado a adicionar
novas funcionalidades e a eliminar erros a uma velocidade espantosa. Esta
versão em si vem com mais de 200 modificações.

Foi realizada uma grande quantidade de trabalho na melhoria do suporte para
áudio. No departamento de "erros resolvidos", viram-se livres de um enorme
consumo de memória, alguma ineficiência nas miniaturas de áudio e
optimizaram o seu armazenamento.

Mas ainda mais excitante é que o Kdenlive agora vem com uma nova mesa de
mistura de som espectacular (veja a imagem). Os programadores também
adicionaram uma nova apresentação de 'clips' de áudio no monitor de 'clips'
e no grupo de projectos, para que possa sincronizar melhor as suas imagens
em movimento com a banda sonora.

{{< img class="text-center" src="Kdenlive1912BETA.png"
link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png"
caption="Mesa de mistura de áudio do Kdenlive" >}}

No departamento da "resolução de pequenos cortes", o Kdenlive ganhou muitas
melhorias de performance e de usabilidade, onde os programadores corrigiram
muitos erros na versão para Windows do editor de vídeo.

## Antevisões e Navegação no [Dolphin](https://userbase.kde.org/Dolphin)

O gestor de ficheiros poderoso do KDE
[Dolphin](https://userbase.kde.org/Dolphin) adiciona novas funcionalidades
para o ajudar a encontrar e a chegar ao que precisa no seu sistema de
ficheiros. Uma dessas funcionalidades é as opções remodeladas de pesquisa
avançada e outra é que agora poderá avançar e recuar no histórico de locais
que visitou várias vezes, carregando de forma prolongada no ícone da seta na
barra de ferramentas. A funcionalidade que lhe permite aceder rapidamente
aos ficheiro gravados ou acedidos recentemente foi remodelada e os seus
problemas foram resolvidos.

Uma das preocupações principais dos programadores do Dolphin é permitir aos
utilizadores anteverem o conteúdo dos ficheiros antes de os abrirem. Por
exemplo, na nova versão do Dolphin, poderá antever os GIF's se os realçar e
depois passar o cursor sobre a área de antevisão. Poderá também carregar e
reproduzir os ficheiros de áudio e vídeo na área de antevisão.

Se for um fã de novelas gráficas, o Dolphin consegue agora mostrar
miniaturas dos livros de banda desenhada .cb7 (consulte o suporte para este
formato de banda desenhada também no Okular -- veja abaixo). E, falando em
miniaturas, a ampliação das miniaturas tem sido algo importante no Dolphin
durante bastante tempo: mantenha carregado o <kbd>Ctrl</kbd>, rode a roda do
rato para que a miniatura aumente ou diminua. Uma nova funcionalidade é que
agora poderá repor a ampliação no nível predefinido se carregar em
<kbd>Ctrl</kbd> + <kbd>0</kbd>.

Outra funcionalidade útil é que o Dolphin lhe mostra quais os programas que
impedem um dispositivo de ser desmontado.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): Deixe o seu
Telemóvel Controlar o seu Ambiente de Trabalho

A última versão do [KDE Connect](https://community.kde.org/KDEConnect) vem
equipada com funcionalidades novas. Uma das mais notórias é que existe uma
nova aplicação de SMS que lhe permite ler e gravar SMS's com o histórico de
conversações completo.

Uma nova interface de utilizador baseada no
[Kirigami](https://kde.org/products/kirigami/) significa que agora consegue
correr o KDE Connect não só no Android, mas também em todas aquelas
plataformas móveis de Linux que iremos ver nos dispositivos de futuro, como
o PinePhone e o Librem 5. A interface em Kirigami também oferece novas
funcionalidades para os utilizadores que usam vários ambientes de trabalho,
como o controlo multimédia, a introdução remota de dados, a activação de
dispositivos, a transferência de ficheiros e a execução de comandos.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="A nova interface no ambiente de trabalho" >}}

Já conseguia usar o KDE Connect para controlar o volume do som a tocar no
seu ambiente de trabalho, mas agora também o pode usar para controlar o
volume global do seu sistema a partir do seu telefone -- o que é bastante
útil quando estiver a usar o seu telefone como um comando à
distância. Enquanto está numa conversa, também poderá controlar a sua
apresentação com o KDE Connect para avançar e recuar os seus 'slides'.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Outra funcionalidade que tem circulado durante algum tempo é a forma como
poderá usar o KDE Connect para enviar um ficheiro para o seu telefone, mas o
que é novo nesta versão é a possibilidade de ter o seu telefone a abrir
imediatamente o ficheiro assim que é recebido. O KDE Itinerary usa isto para
enviar informações de viagens para o seu telefone a partir do KMail.

Numa nota associada, poderá agora também enviar ficheiros a partir das
aplicações Thunar (o gestor de ficheiros do Xfce) e Elementary, como o
Pantheon Files. A apresentação da evolução da cópia de vários ficheiros foi
bastante melhorada e os ficheiros recebidos a partir do Android apresentam
agora a sua data de modificação correcta.

No que diz respeito às notificações, poderá agora desencadear acções a
partir das notificações do Android a partir do ambiente de trabalho e o KDE
Connect usa as funcionalidades avançadas do centro de notificações do Plasma
para oferecer melhores notificações. A notificação de confirmação do
emparelhamento não expira o tempo-limite, pelo que não a irá perder mais.

Finalmente, se estiver a usar o KDE Connect a partir da linha de comandos, o
*kdeconnect-cli* agora oferece a completação automática do *zsh*.

## Imagens Remotas [Gwenview](https://userbase.kde.org/Gwenview)

O [Gwenview](https://userbase.kde.org/Gwenview) agora permite-lhe ajustar o
nível de compressão do JPEG quando gravar as imagens que editou. Os
programadores também melhoraram a performance das imagens remotas e o
Gwenview consegue agora importar as fotografias de e para localizações
remotas.

## O [Okular](https://kde.org/applications/office/org.kde.okular)

O [Okular](https://kde.org/applications/office/org.kde.okular), o
visualizador de documentos que lhe permite ler, anotar e verificar todos os
tipos de documentos, incluindo os PDFs, EPubs, ODTs e MDs, agora também
suporta os ficheiros que usam o formato de bandas desenhadas em .cb7.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): Leitor de Músicas Simples mas Completo

O [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) é um leitor
de música do KDE que combina a simplicidade com uma interface moderna e
elegante. Na sua última versão, o Elisa optimizou o seu visual para se
adaptar melhor aos ecrãs de DPI's elevados. Também se integra melhor com as
outras aplicações do KDE e adquiriu o suporte para o sistema de menus
globais do KDE.

A indexação de ficheiros de música foi também melhorada e o Elisa agora
suporta também rádios na Web e vem com alguns exemplos para você
experimentar.

{{< img src="elisa.png" caption="Interface actualizada do Elisa" >}}

## O [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) Está Pronto para Ecrãs Tácteis

O [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), o
programa predefinido do KDE para capturas do ecrã, é outra aplicação do KDE
que tem evoluindo para adquirir capacidades para ecrãs tácteis: o seu
processamento de arrastamento amigável em ecrãs tácteis torna muito mais
simples a selecção de rectângulos para capturar as partes do ecrã que deseja
nos dispositivos com ecrãs tácteis.

Outras melhorias são a nova funcionalidade de gravação automática, que é
bastante útil para tirar rapidamente várias fotografias e as barras de
progresso animadas, que garantem que você consegue ver claramente o que o
Spectacle está a fazer.

{{<img style="max-width:500px" src="spectacle.png" caption="Novo tratamento da pega do Spectacle" >}}

## Melhorias na [Integração de Navegadores do Plasma](https://community.kde.org/Plasma/Browser_Integration)

A nova versão da [Integração de Navegadores com o Plasma](https://community.kde.org/Plasma/Browser_Integration)
agora oferece uma lista de proibições
para a funcionalidade dos controlos multimédia. Isto é útil quando visitar
uma página com uma grande quantidade de conteúdos que poderá reproduzir, o
que se poderá tornar difícil para seleccionar o que deseja controlar a
partir do seu ambiente de trabalho. A lista de proibições permite-lhe
excluir estas páginas nos controlos multimédia.

A nova versão também lhe permite manter o URL de origem nos meta-dados dos
ficheiros e adiciona o suporte para a API de Partilhas na Web. Esta API
permite aos navegadores partilharem ligações, texto e ficheiros com outras
aplicações, da mesma forma que as aplicações do KDE fazem, melhorando a
integração com navegadores não-nativos como o Firefox, o Chrome/Chromium e o
Vivaldi, com o resto das aplicações do KDE.

{{<learn-more
href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Loja da Microsoft

Muitas das aplicações do KDE podem ser obtidas a partir da Loja da
Microsoft. O Krita e o Okular têm lá estado há algum tempo e têm agora a
acompanhá-las o [Kile](https://kde.org/applications/office/org.kde.kile), um
editor de LaTeX amigável.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Novo

O que nos traz às novidades:
[SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), uma
aplicação que lhe permite criar facilmente legendas para vídeos, está agora
no KDE Incubator, seguindo o processo para se tornar um membro pleno da
família de aplicações do KDE. Bem-vinda!

Entretanto o [plasma-nano](https://invent.kde.org/kde/plasma-nano), uma
versão mínima do Plasma desenhada para dispositivos móveis, foi migrada para
os repositórios do Pasma, estando pronta a ser lançada com o 5.18.

## Versão 19.12

Alguns dos nossos projectos são lançados de acordo com as suas próprias
agendas e alguns são lançados em massa. A versão 19.12 dos projectos foi
lançada hoje e deverá estar disponível através das lojas de aplicações e
distribuições em breve. Consulte a [página da versão 19.12](https://www.kde.org/announcements/releases/19.12).
 Este pacote foi chamado anteriormente de
Aplicações do KDE mas tem vindo a perder a sua marca para se tornar um
serviço de lançamento de versões, para evitar confusões com todas as outras
aplicações do Kde e porque consiste em dezenas de diferentes produtos em vez
de um único grupo.
